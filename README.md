Insider - Frontend Coding Challenge

## Installation and Getting Started

Installing the project is pretty standard with Node (`npm`). So, right after cloning the project, install the dependencies by running;

`>> npm install`

### Available Scripts (Frontend)

Start the app using: 

`>> npm run serve`

Open [http://localhost:8080](http://localhost:8080) to view it in the browser. (The page will reload if you make edits)

### Technology Stack

* Vue.js
* Vuex

### GIT Strategy

Demonstration of GIT Flow model with clean commit history, with master as `production` branch, `dev` for development. Later branches can be made from `dev` as `feature/TICKET_NUMBER-XXX` or `bug/TICKET_NUMBER-XXX` or maybe from `production` for hotfixes.

### Important Notes
Tests and stylings are not covered in this project due to time limits and was not the part of requirement.

### Author
Hamza Khan - Frontend Engineer