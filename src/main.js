// modules
import Vue from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPause, faPlay } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// stores
import store from './store/store'

// configurations and initializations
library.add(faPause, faPlay);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
    store,
    render: h => h(App)
}).$mount('#app');