// app general constants
export const TEETER_TOTTER_WIDTH = 10; // 10 meters
export const SHAPE_COUNT = 3; // 3 types of shapes available
export const MAX_WEIGHT = 10;
export const MIN_WEIGHT = 1;
export const MAX_BENDING = 30; // max angle of swing bar
export const MIN_BENDING = -30; // min angle of swing bar
export const MAX_SIDES_DIFFERENCE = 20;
export const FALLING_BLOCKS_COUNT = 2;
export const INITIAL_TIMEOUT = 300;
export const TIMEOUT_STEP_DECREASING = 50;
export const ITERATION_COUNT_INCREASING = 5;

// events
export const eventConstants = {
    FINISHED_FALLING: "finishedFalling"
}

// vuex actions
export const ON_TOGGLE_PAUSE = 'ON_TOGGLE_PAUSE';
export const ON_ADD_RIGHT_SIDE_BLOCK = 'ON_ADD_RIGHT_SIDE_BLOCK';
export const ON_ADD_LEFT_SIDE_BLOCK = 'ON_ADD_LEFT_SIDE_BLOCK';
export const ON_RESET_STATE = 'ON_RESET_STATE';
export const ON_FINISH_FALLING = 'ON_FINISH_FALLING';
export const ON_START_NEW_GAME = 'ON_START_NEW_GAME';
export const ON_INITIALIZE_FALLING_BLOCKS = 'ON_INITIALIZE_FALLING_BLOCKS';
export const ON_ADD_FALLING_BLOCK = 'ON_ADD_FALLING_BLOCK';
export const ON_MOVE_RIGHT = 'ON_MOVE_RIGHT';
export const ON_MOVE_LEFT = 'ON_MOVE_LEFT';